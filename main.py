from plot_map import plot_map, _generate_map
from db_filter import load_amr_genes



## Generate map figure and re-use to _substantially_ save time
## (and memory use apparently)
map_fig, map_ax = _generate_map()


## Plot map with number of sequences overall
plot_map('samples_map.eps', (map_fig, map_ax))

## Plot map showing AMR genes found, one map per gene
AMR_GENES = load_amr_genes()
for GENE in AMR_GENES:
    """ 
        Memory use for map generation peaks at ~9GB,
        so as much as I'd like to I cannot use multiprocessing
        with 16GB of memory.
    """
    plot_map('per_gene/' + GENE + '_map.eps', (map_fig, map_ax), GENE)
