import openpyxl
import pgeocode


# SRC FILES
POSTCODE_DB = str('./src/postcodes_db.xlsx')
SAMPLES_DB = str('./src/samples_db.xlsx')



def _import_postcode_db(POSTCODE_DB: str) -> dict:
    """
        Load excel file containing the relationship between farm codes
        and postcodes, and create a dictionary.
    """
    FARM_ID = 0
    FARM_PC = 3
    DB = openpyxl.load_workbook(POSTCODE_DB)  # .active points to first ws: DONT
    ws = DB.worksheets[1]  #  This is worksheet 'Farms'
    POSTCODES = dict()
    for FARM in ws:
        if FARM[FARM_ID].value == None:
            break
        elif len(FARM[FARM_ID].value) == 3:  # FARM_ID is a 3-letter code
            POSTCODES[FARM[FARM_ID].value] = FARM[FARM_PC].value
    return POSTCODES


def _import_samples_db(SAMPLES_DB: str, COL_NAME: str=None) -> dict:
    """
        Load excel file containing all samples processed, and create a
        dictionary with isolate ID and type of sample.
    """
    ISOLATE_ID = 1
    
    DB = openpyxl.load_workbook(SAMPLES_DB)
    ws = DB.worksheets[0]  # Data in Sheet1
    
    if COL_NAME == None:
        # Defaults to Farm type
        COLUMN_ID = 2
    else:
        for header_row in ws:
                # this extract just first row
                break
            
        headers = [i.value.upper() for i in header_row if i.value is not None]
        COLUMN_ID = headers.index(COL_NAME.upper())
    
    DATA = dict()
    for SAMPLE in ws: 
        if SAMPLE[ISOLATE_ID].value == None:
            break
        elif SAMPLE[ISOLATE_ID].value != 'ISOLATE':
            DATA[SAMPLE[ISOLATE_ID].value] = SAMPLE[COLUMN_ID].value
    return DATA


def _translate_postcodes(POSTCODES: dict) -> dict:
    """
        Translate postcodes into longitude (~x) & latitude (~y)
    """
    GIS_DATA = dict()
    nomi = pgeocode.Nominatim('gb_full')  # Country: 'gb'
    for FARM_ID in POSTCODES:
        report = nomi.query_postal_code(POSTCODES[FARM_ID])
        GIS_DATA[FARM_ID] = [report.longitude, report.latitude]
    return GIS_DATA


def load_sample_data() -> dict:
    """
    """
    POSTCODES = _import_postcode_db(POSTCODE_DB)
    GIS_DATA = _translate_postcodes(POSTCODES)
    DATA = _import_samples_db(SAMPLES_DB)
    return DATA, POSTCODES, GIS_DATA


def load_amr_genes() -> list:
    """
        Screens SAMPLE_DB spreadsheet and 
    """
    DB = openpyxl.load_workbook(SAMPLES_DB)
    ws = DB.worksheets[0]  # Data in Sheet1
    
    SKIP_HDR = 5  # Number of annotations prior to listing ABX
    SKIP_VIR = -1  # Position of 'Virulence' header
    for header_row in ws:
        # this extract just first row
        break
    
    headers = [i.value.upper() for i in header_row if i.value is not None]
    return headers[SKIP_HDR:SKIP_VIR]


def gene_counter(GENE_NAME: str=None) -> list:
    """
    """
    return _import_samples_db(SAMPLES_DB, GENE_NAME)