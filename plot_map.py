import matplotlib.pyplot as plt
from cartopy.io import img_tiles 
from cartopy import crs
from cartopy import feature as feat
import pgeocode

from db_filter import load_sample_data, gene_counter



## Retrieve list of postal codes and transform them into LONG / LAT coordinates
DATA, POSTCODES, GIS_DATA = load_sample_data()


def _generate_map():
    """
    """
    ## Load map to plot the above coordinates
    fig = plt.figure(figsize=(8,8))
    ax = fig.add_subplot(1, 1, 1, projection=crs.PlateCarree())

    # [LONG_0, LONG_1, LAT_0, LAT_1] Coordinates set for Wales
    ax.set_extent([-5.5, -2.25, 51.1, 53.5], crs.PlateCarree())
    ax.add_image(img_tiles.OSM(cache=True), 8)  # Requires inet comms
    ax.add_feature(feat.LAND)
    ax.add_feature(feat.OCEAN)
    ax.add_feature(feat.LAKES)

    ax.coastlines(resolution='10m', color='black', linewidth=0.5)  # Use '110m' for lower resolution / faster plot
    ax.gridlines(draw_labels=True)
    return fig, ax


def plot_map(FIG_NAME: str, FIG_METADATA: tuple, GENE_NAME: list=None):
    """
    """
    fig, ax = FIG_METADATA
    ## Plot locations here using LONG,LAT coordinates as X,Y
    annotations = list()
    for FARM_ID in GIS_DATA:
        LONG, LAT = GIS_DATA[FARM_ID]
        SAMPLES = [DATA[FARM] for FARM in DATA.keys() if FARM_ID in FARM]

        if len(SAMPLES) > 0 and len(SAMPLES) == SAMPLES.count(SAMPLES[0]):
            if SAMPLES[0] == 'D':
                FF_COLOUR = 'black'
                FE_COLOUR = 'black'
                pD = ax.plot([1, 1], [1, 1], marker='s', markersize=6.5,
                        markerfacecolor=FF_COLOUR, markeredgecolor=FE_COLOUR,
                        linewidth=0.5, linestyle='None', label='Diary')
            elif SAMPLES[0] == 'B':
                FF_COLOUR = 'chocolate'
                FE_COLOUR = 'saddlebrown'
                pB = ax.plot([1, 1], [1, 1], marker='s', markersize=6.5,
                        markerfacecolor=FF_COLOUR, markeredgecolor=FE_COLOUR,
                        linewidth=0.5, linestyle='None', label='Cattle')
            elif SAMPLES[0] == 'S':
                FF_COLOUR = 'white'
                FE_COLOUR = 'lightgrey'
                pS = ax.plot([1, 1], [1, 1], marker='s', markersize=6.5,
                        markerfacecolor=FF_COLOUR, markeredgecolor=FE_COLOUR,
                        linewidth=0.5, linestyle='None', label='Sheep')
        elif len(SAMPLES) > 0:  # Mixed: B+S (X)
            FF_COLOUR = 'darkgrey'
            FE_COLOUR = 'grey'
            pM = ax.plot([1, 1], [1, 1], marker='s', markersize=6.5,
                    markerfacecolor=FF_COLOUR, markeredgecolor=FE_COLOUR,
                    linewidth=0.5, linestyle='None', label='Farms w/ mixed animals')
        
        # Plot farm location
        ax.plot(LONG, LAT, marker='s', markersize=6.5, markerfacecolor=FF_COLOUR,
                markeredgecolor=FE_COLOUR, linewidth=0.5)
        
        # Plot farm metadata
        if GENE_NAME == None:
            if FF_COLOUR == 'white':
                txt_ann = ax.text(LONG, LAT, len(SAMPLES), color='black',
                                  ha='center', va='center_baseline',
                                  fontsize=5.5, fontweight='bold')
            else:
                txt_ann = ax.text(LONG, LAT, len(SAMPLES), color='white',
                                  ha='center', va='center_baseline', 
                                  fontsize=5.5, fontweight='bold')
        else:  # GENE_NAME == something
            FARM_LIST = gene_counter(GENE_NAME)
            GENES = [f for f in FARM_LIST if FARM_LIST[f] is not None \
                        and FARM_ID in f]
            if FF_COLOUR == 'white':
                txt_ann = ax.text(LONG, LAT, len(GENES), color='black',
                                   ha='center', va='center_baseline',
                                   fontsize=5.5, fontweight='bold')
            else:
                txt_ann = ax.text(LONG, LAT, len(GENES), color='white',
                                  ha='center', va='center_baseline',
                                  fontsize=5.5, fontweight='bold')
        
        annotations.append(txt_ann)
    # TODO: some postcodes are different but extremelly close in coordinates.

    # Show legend
    ax.legend(handles=[pD[0], pB[0], pS[0], pM[0]], loc='lower left')

    if FIG_NAME.count('.') == 1:
        FIG_EXT = FIG_NAME.split('.')[1]
    else:
        FIG_EXT = FIG_NAME.split('.')[2]
    
    fig.savefig('./img/' + FIG_NAME, format=FIG_EXT, bbox_inches='tight')
    
    # Clear text to re-use figure
    for txt in annotations:
        txt.remove()